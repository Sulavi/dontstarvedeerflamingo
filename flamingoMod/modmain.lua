local FOODTYPE = GLOBAL.FOODTYPE
local CHARACTER_INGREDIENT = GLOBAL.CHARACTER_INGREDIENT
local AllRecipes = GLOBAL.AllRecipes
local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
local TECH = GLOBAL.TECH

local STRINGS = GLOBAL.STRINGS

PrefabFiles = {
"deerflamingo",
}

Assets = {
Asset( "ATLAS", "images/inventoryimages/deerflamingo.xml" ),
Asset( "IMAGE", "images/inventoryimages/deerflamingo.tex" ),
}

----------------------------------------------------------------------
--Ingredients: frogglebunwich feather_robin

AddRecipe("critter_deerflamingo_builder", 
{
Ingredient("cutgrass", 1),
Ingredient("berries", 1)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/deerflamingo.xml", 
"deerflamingo.tex")

STRINGS.NAMES.CRITTER_DEERFLAMINGO_BUILDER = "Flamingo Deer"
STRINGS.RECIPE_DESC.CRITTER_DEERFLAMINGO_BUILDER = "Adopt an adorable Flamingo Deer"

----------------------------------------------------------------------

STRINGS.NAMES.CRITTER_DEERFLAMINGO = "Flamingo Deer"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_DEERFLAMINGO = "It's a Flamingo Deer"
