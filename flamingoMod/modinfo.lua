-- This information tells other players more about the mod
name = "Flamingo Deer Pet"
description = "Adds a new craftable Critter to the game: the Flamingo Deer.\n\nArt and creature design by yogin.\nhttps://twitter.com/yoginnnnnn/status/1243577056136437760"
author = "Sulavi"
version = "1.0" -- This is the version of the template. Change it to your own number.

-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "none"


-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

-- Compatible with only Don't Starve Together
dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false

-- Character mods need this set to true
all_clients_require_mod = true 

client_only_mod = false

icon_atlas = "modicon.xml"
icon = "modicon.tex"

-- The mod's tags displayed on the server list
server_filter_tags = {
	"deer",
	"flamingo",
	"deer flamingo",
	"pet",
	"critter"
}

--configuration_options = {}