

require("stategraphs/commonstates")

local actionhandlers =
{
  ActionHandler(ACTIONS.GOHOME, function(inst)
        local ba = inst:GetBufferedAction()
        if ba and ba.target and ba.target:HasTag("hoodiniden") then
            return "flyaway"
        else
            return "action"
        end
    end)
}

local events =
{
	
    CommonHandlers.OnStep(),
    CommonHandlers.OnLocomote(false, true),
    EventHandler("attacked", function(inst)
        if inst.components.health and not inst.components.health:IsDead() then
            inst.sg:GoToState("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/hit")
        end
    end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
}

local states =
{
    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
			inst:AddTag("poofed")
        end,

        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/characters/owlsounds/hurt") end),
          --  TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/walk") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
        end,

        events =
        {
         --   EventHandler("animover", function(inst) inst:ondeath() end),
        },
    },

	State{

        name = "action",
        onenter = function(inst, playanim)
            inst.Physics:Stop()
         --   inst.AnimState:PlayAnimation("fly_loop", true)
            inst:PerformBufferedAction()
        end,
        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
   State{
        name = "flyaway",
        tags = {"flight", "busy"},
        onenter = function(inst)
            inst.Physics:Stop()

            inst.DynamicShadow:Enable(false)
            inst.components.health:SetInvincible(true)

        --    inst.AnimState:PlayAnimation("fly_away_pre")
        --    inst.AnimState:PushAnimation("fly_away_loop", true)

            inst.Physics:SetMotorVel(0,10+math.random()*2,0)
        end,

        onupdate = function(inst)
            inst.Physics:SetMotorVel(0,10+math.random()*2,0)
        end,

        timeline = {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(41*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(51*FRAMES, function(inst) inst:PerformBufferedAction() end ),
        },

    },

    State{
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/hit")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

CommonStates.AddWalkStates(states,
{
    walktimeline =
    {
        TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/footstep") end),
        TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/footstep") end),
    }
})

return StateGraph("hoodini", states, events, "idle")

