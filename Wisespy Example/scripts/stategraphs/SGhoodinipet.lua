

require("stategraphs/commonstates")

SGCritterEvents = {}
SGCritterStates = {}

local actionhandlers =
{
  ActionHandler(ACTIONS.GOHOME, function(inst)
        local ba = inst:GetBufferedAction()
        if ba and ba.target and ba.target:HasTag("hoodiniden") then
            return "flyaway"
        else
            return "action"
        end
    end)
}

local events =
{
	
    CommonHandlers.OnStep(),
    CommonHandlers.OnLocomote(false, true),
    EventHandler("attacked", function(inst)
        if inst.components.health and not inst.components.health:IsDead() then
            inst.sg:GoToState("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/hit")
        end
    end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
}
--[[
SGCritterStates.AddNuzzle(states, actionhandlers,
{
TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundsdon.grunt) end),
})
--]]
SGCritterStates.AddNuzzle = function(states, actionhandlers, timeline, fns)
    table.insert(actionhandlers, ActionHandler(ACTIONS.NUZZLE, "nuzzle"))

    table.insert(states, State
    {
		name = "nuzzle",
		tags = {"busy", "nuzzled"},

		onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
				inst:PushEvent("nuzzled")
			inst.sg:GoToState("nuzzled") 
				inst:AddTag("nuzzled")
            end
            
            
            inst.sg.mem.prevnuzzletime = GetTime()

            if fns ~= nil and fns.onenter ~= nil then
                fns.onenter(inst)
				inst.AnimState:PlayAnimation("hit_down")
            end
		end,

		onexit = function(inst)
			inst:PerformBufferedAction()
			inst:ClearBufferedAction()
		end,

		timeline = timeline,

		events =
		{
			EventHandler("animover", function(inst) 
                if inst.AnimState:AnimDone() then
					--inst:PushEvent("critter_onnuzzle")
					--inst.sg:GoToState("idle") 
				end
			end)
		},

        onexit = fns ~= nil and fns.onexit or nil,
    })
end

SGCritterStates.AddPetEmote = function(states, timeline, onexit)
    table.insert(states, State
    {
		name = "emote_pet",
		tags = {"busy", "nuzzled"},

		onenter = function(inst)
            inst.AnimState:PlayAnimation("hit_down")
			inst:PushEvent("nuzzled")
			inst.sg:GoToState("nuzzled") 
			inst:AddTag("nuzzled")
		end,

		timeline = timeline,

		events =
		{
			EventHandler("animover", function(inst) 
                if inst.AnimState:AnimDone() then
				--	inst:PushEvent("nuzzled")
				--	inst.sg:GoToState("idle") 
				end
			end)
		},
		
		onexit = onexit,
    })
end

local states =
{
    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
			inst:AddTag("poofed")
        end,

        timeline = 
        {
            --TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("owlsounds/owlsounds/hoot") end),
      --      TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("owlsounds/owlsounds/hoot") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
        end,

        events =
        {
         --   EventHandler("animover", function(inst) inst:ondeath() end),
        },
    },

	State{

        name = "action",
        onenter = function(inst, playanim)
            inst.Physics:Stop()
         --   inst.AnimState:PlayAnimation("fly_loop", true)
            inst:PerformBufferedAction()
        end,
        events=
        {
            EventHandler("animover", function (inst)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
   State{
        name = "flyaway",
        tags = {"flight", "busy"},
        onenter = function(inst)
            inst.Physics:Stop()

            inst.DynamicShadow:Enable(false)
            inst.components.health:SetInvincible(true)

        --    inst.AnimState:PlayAnimation("fly_away_pre")
        --    inst.AnimState:PushAnimation("fly_away_loop", true)

            inst.Physics:SetMotorVel(0,10+math.random()*2,0)
        end,

        onupdate = function(inst)
            inst.Physics:SetMotorVel(0,10+math.random()*2,0)
        end,

        timeline = {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(41*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(51*FRAMES, function(inst) inst:PerformBufferedAction() end ),
        },

    },

 State{
        name = "emote_pet",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/birds/wingflap_cage")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,

timeline = {
             TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
			 TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("owlsounds/owlsounds/hoot") end ),
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bat/flap") end ),
           -- TimeEvent(51*FRAMES, function(inst) inst:PerformBufferedAction() end ),
        },
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    State{
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/hit")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

CommonStates.AddWalkStates(states,
{
    walktimeline =
    {
        TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/footstep") end),
        TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/mandrake/footstep") end),
    }
})

return StateGraph("hoodinipet", states, events, "idle")

