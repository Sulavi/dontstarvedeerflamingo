local assets =
{
    Asset("ANIM", "anim/cave_entrance.zip"),
    Asset("ANIM", "anim/ruins_entrance.zip"),
	Asset("MINIMAP_IMAGE", "cave_closed"),
	Asset("MINIMAP_IMAGE", "cave_open"),
	Asset("MINIMAP_IMAGE", "cave_no_access"),
	Asset("MINIMAP_IMAGE", "cave_overcapacity"),
	Asset("MINIMAP_IMAGE", "ruins_closed"),
}

local prefabs =
{
    "bat",
    "rock_break_fx",
}

local function close(inst)
    inst.AnimState:PlayAnimation("no_access", true)
end

local function open(inst)
    inst.AnimState:PlayAnimation("open", true)
end

local function full(inst)
    inst.AnimState:PlayAnimation("over_capacity", true)
end

--local function activate(inst)
    -- nothing
--end

local function ReturnChildren(inst)
    for k, child in pairs(inst.components.childspawner.childrenoutside) do
        if child.components.homeseeker ~= nil then
            child.components.homeseeker:GoHome()
        end
		child.components.follower:StopFollowing()
        child:PushEvent("gohome")
    end
end

local function OnIsDay(inst, isday)
    if isday then
        inst.components.childspawner:StartRegen()
        inst.components.childspawner:StopSpawning()
        ReturnChildren(inst)
		inst.AnimState:PlayAnimation("owly_tree_empty")
    else
        inst.components.childspawner:StopRegen()
        inst.components.childspawner:StartSpawning()
		inst.AnimState:PlayAnimation("owly_tree")
    end
end

local function canspawn(inst)
   -- return inst.components.worldmigrator:IsActive() or inst.components.worldmigrator:IsFull()
end

local function activatebyother(inst)
    OnWork(inst, nil, 0)
end

local function onhammered(inst)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end

 inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/livingtree_hit")
 
inst.components.lootdropper:DropLoot()
SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition()) 
inst.SoundEmitter:PlaySound("dontstarve/forest/treeCrumble")
 inst.components.childspawner:ReleaseAllChildren()
inst:DoTaskInTime(0.1, function() inst:Remove() end)
end

local function onhit(inst, chopper, workleft)
local fx = SpawnPrefab("maxwell_smoke")
local shadow = SpawnPrefab("shadow_despawn")
--fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
inst.AnimState:PlayAnimation("owly_tree_empty_hit")
 if not (chopper ~= nil and chopper:HasTag("playerghost")) then
        inst.SoundEmitter:PlaySound(
            chopper ~= nil and chopper:HasTag("beaver") and
            "dontstarve/characters/woodie/beaver_chop_tree" or
            "dontstarve/wilson/use_axe_tree"
        )
    end
		
end

local function fn(bank, build, anim, minimap, isbackground)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst.MiniMapEntity:SetIcon("evergreen.png")

    inst.AnimState:SetBuild("greatlivingtree")
    inst.AnimState:SetBank("greatlivingtree")
    inst.AnimState:PlayAnimation("owly_tree")
	inst.Transform:SetScale(1.7, 2.0, 1.7)	
	
	inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.CHOP)
    inst.components.workable:SetWorkLeft(20)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	
	 inst:AddComponent("lootdropper")
 inst.components.lootdropper:SetLoot({"log", "log", "livinglog"})


    inst:AddTag("antlion_sinkhole_blocker")
    inst:AddTag("hoodiniden")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    return inst
end

local function onspawnchild(inst)
inst.AnimState:PlayAnimation("owly_tree_empty")
end
local function childgohome(inst)
inst.AnimState:PlayAnimation("owly_tree")
end

local function open_fn()
local inst = fn()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("childspawner")
    inst.components.childspawner:SetRegenPeriod(60)
    inst.components.childspawner:SetSpawnPeriod(.1)
    inst.components.childspawner:SetMaxChildren(1)
    inst.components.childspawner:SetSpawnedFn( onspawnchild )
    inst.components.childspawner:SetGoHomeFn( childgohome )
    inst.components.childspawner.childname = "hoodini"

   -- inst.components.inspectable.getstatus = GetStatus PushEvent


    --inst:ListenForEvent("migration_activate", activate)

    --Cave entrance is an overworld entity, so it
    --should be aware of phase and not cavephase.
    --Actually, it could make sense either way...
    --Alternative: -add "cavedweller" tag
    --             -watch iscaveday world state
    OnIsDay(inst, TheWorld.state.isday)
    inst:WatchWorldState("isday", OnIsDay)

    return inst
end

return Prefab("hoodiniden", open_fn, assets, prefabs)
