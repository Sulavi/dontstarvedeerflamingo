require "brains/perdbrain"
require "stategraphs/SGperd"

local THUNDERBIRD_RUN_SPEED = 5.5
local THUNDERBIRD_WALK_SPEED = 2

local assets=
{
	--Asset("ANIM", "anim/perd_basic.zip"),
	Asset("ANIM", "anim/hoodini.zip"),
	Asset("SOUNDPACKAGE", "sound/owlsounds.fev"),
    Asset("SOUND", "sound/owlsounds.fsb"),
}

local prefabs =
{
    "drumstick",
 --   "feather_thunder",
    "thunderbird_fx",
}

local loot = 
{
    "drumstick",
    "drumstick",
 --   "feather_thunder"
}

local function ReturnHome(inst)
	local player2 = FindClosestPlayerToInst(inst, 900, true)
	
		if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	
SpawnPrefab("slurper_respawn").Transform:SetPosition(inst.Transform:GetWorldPosition())
inst.components.follower:StopFollowing()
inst:PushEvent("gohome")
inst.components.follower:StopFollowing()
inst.components.follower:SetLeader(nil)

    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    if home ~= nil and home:IsValid() and home.components.childspawner ~= nil then
        home.components.childspawner:GoHome(inst)
    end
	

	
end

local function OnSleepGoHome(inst)
    inst._hometask = nil
    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    if home ~= nil and home:IsValid() and home.components.childspawner ~= nil then
        home.components.childspawner:GoHome(inst)
    end
end

local function Hoot(inst)
local chance = math.random(1,3)
		if chance ==1 then
inst.SoundEmitter:PlaySound("owlsounds/owlsounds/hoot")
end
end
--SetLeader
local function StalkLeader(inst)

	local is_busy = inst.sg:HasStateTag("busy")
	local is_idling = inst.sg:HasStateTag("idle")
    local is_moving = inst.sg:HasStateTag("moving")
	if is_moving then 
	inst.AnimState:SetMultColour(0.0, 0.0, 0.0, 0.1)
	inst.DynamicShadow:SetSize(0, 0)
	elseif not is_busy then
	inst.AnimState:SetMultColour(1.0, 1.0, 1.0, 1.0)	
	inst.DynamicShadow:SetSize(2, 1)
	end
	
    local player = FindClosestPlayerToInst(inst, 8, true)
    local player1 = FindClosestPlayerToInst(inst, 14, true)
    local player2 = FindClosestPlayerToInst(inst, 40, true)
	local player3 = FindClosestPlayerToInst(inst, 40, true)
    if player ~= nil and not is_moving and not inst:HasTag("dead") then
	if player.components.sanity then
	if not inst:HasTag("mindcontrolling") then
	inst.components.follower:SetLeader(player)
		if player.components.sanity.current < (100) and inst:HasTag("mad")  then
 --       player.AnimState:PlayAnimation("mindcontrolled_loop")
        inst.AnimState:PlayAnimation("hit")
		inst.SoundEmitter:PlaySound("owlsounds/owlsounds/hoot")
		inst:AddTag("mindcontrolling")
		player3:AddTag("mindcontrolledplayer")
		player3.components.health:DoDelta(10)
		inst:DoTaskInTime(15, function(inst) inst:RemoveTag("mindcontrolling") end)
		inst:DoTaskInTime(0.1, function(inst) player.sg:GoToState("mindcontrolled_loop") end)
    --    player.sg:GoToState("mindcontrolled_loop")
		player.components.sanity:SetInducedInsanity(inst, true)
		
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(player.Transform:GetWorldPosition())
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(player.Transform:GetWorldPosition())
		SpawnPrefab("cane_ancient_fx").Transform:SetPosition(player.Transform:GetWorldPosition())


    end
    end
    end
   
    
	else
	if is_moving and not player1 then
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2:RemoveTag("mindcontrolledplayer")
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	inst.components.follower:StopFollowing()
inst:PushEvent("gohome")
inst.components.follower:StopFollowing()
inst.components.follower:SetLeader(nil)

    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    if home ~= nil and home:IsValid() and home.components.childspawner ~= nil then
        home.components.childspawner:GoHome(inst)
    end
    end
    end



end


local function DoLightning(inst, target)
    local LIGHTNING_COUNT = 3
    local COOLDOWN = 60

    for i=1, LIGHTNING_COUNT do
        inst:DoTaskInTime(0.4*i, function ()
            local rad = math.random(4, 8)
            local angle = i*((4*PI)/LIGHTNING_COUNT)
            local pos = Vector3(target.Transform:GetWorldPosition()) + Vector3(rad*math.cos(angle), 0, rad*math.sin(angle))
            TheWorld:PushEvent("ms_sendlightningstrike", pos) 
        end)
    end

    inst.cooling_down = true
    inst:DoTaskInTime(COOLDOWN, function () inst.cooling_down = false end)
end

local function onnear(inst)
local player = GetClosestInstWithTag("player", inst, 10)
local flower = GetClosestInstWithTag("hoodiniden", inst, 9999)
local hoodiniden = GetClosestInstWithTag("hoodiniden", inst, 9999)
local tp_pos = flower:GetPosition() 
local tp_pos2 = hoodiniden:GetPosition() 
if not inst:HasTag("dead") then
player.components.sanity:SetInducedInsanity(inst, false)
if player ~= nil and inst:HasTag("mad") then 
SpawnPrefab("shadow_puff_large_front").Transform:SetPosition(inst.Transform:GetWorldPosition())
SpawnPrefab("shadow_puff_large_back").Transform:SetPosition(inst.Transform:GetWorldPosition())
SpawnPrefab("shadow_shield1").Transform:SetPosition(inst.Transform:GetWorldPosition())
player.components.health:DoDelta(-5)
player.components.sanity:DoDelta(-5)
player.sg:GoToState("hit")
inst.sg:GoToState("hit")
SpawnPrefab("slurper_respawn").Transform:SetPosition(inst.Transform:GetWorldPosition())
inst.components.follower:StopFollowing()
inst:PushEvent("gohome")
inst.components.follower:StopFollowing()
inst.components.follower:SetLeader(nil)

    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    if home ~= nil and home:IsValid() and home.components.childspawner ~= nil then
        home.components.childspawner:GoHome(inst)
    end
	
end

if not inst:HasTag("mad") and flower ~= nil then
SpawnPrefab("slurper_respawn").Transform:SetPosition(inst.Transform:GetWorldPosition())
inst:AddTag("mad")
inst:DoTaskInTime(0.1, function(inst) inst.Physics:Teleport(tp_pos:Get()) end)
end		
		
end
end

local function onfar(inst)

end

local function spawnfx(inst)
    if not inst.fx then
        inst.fx = SpawnPrefab("thunderbird_fx")
        local x,y,z = inst.Transform:GetWorldPosition()
        inst.fx.Transform:SetPosition(x, y, z)

        local follower = inst.fx.entity:AddFollower()
        follower:FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, 0, 0 )
        inst.fx:FacePoint(inst.Transform:GetWorldPosition())
   end
end

local function OnDeath (inst, player)
inst:AddTag("dead")
inst.components.lootdropper:DropLoot()
	local player2 = FindClosestPlayerToInst(inst, 900, true)
	
		if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
	if player2 ~= nil then
	player2.components.sanity:SetInducedInsanity(inst, false)
    end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local sound = inst.entity:AddSoundEmitter()
	local shadow = inst.entity:AddDynamicShadow()
	
	MakeCharacterPhysics(inst, 50, .5)
inst.Transform:SetScale(0.76, 0.76, 0.76)

    inst.DynamicShadow:SetSize(2, 1)
    inst.Transform:SetFourFaced()
    inst.entity:AddNetwork()


--    MakePoisonableCharacter(inst)
     
    anim:SetBank("hoodini")
    anim:SetBuild("hoodini")
	anim:PlayAnimation("idle", true)
    anim:Hide("hat")

    --inst:AddTag("character")
    inst:AddTag("follow")
    inst:AddTag("berrythief")
 
    inst.entity:SetPristine()
inst:DoPeriodicTask(5, Hoot)
	if not TheWorld.ismastersim then
		return inst
	end
 
  --  inst:AddComponent("eater")
  --  inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
--    table.insert(inst.components.eater.foodprefs, "RAW")
--    table.insert(inst.components.eater.ablefoods, "RAW")
    
   -- inst:AddComponent("sleeper") SGthunderbird
 --   inst.components.sleeper:SetWakeTest( function() return true end)    --always wake up if we're asleep

    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "pig_torso"
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.PERD_HEALTH)
    inst.components.combat:SetDefaultDamage(TUNING.PERD_DAMAGE)
    inst.components.combat:SetAttackPeriod(3)

    inst:AddComponent("inventory")
    inst:AddComponent("inspectable")

    inst:AddComponent("playerprox")
    inst.components.playerprox:SetDist(4, 5)
    inst.components.playerprox:SetOnPlayerNear(onnear)
    inst.components.playerprox:SetOnPlayerFar(onfar)
	
    inst:AddComponent("locomotor")
    inst.components.locomotor.runspeed = 4
    inst.components.locomotor.walkspeed = 5
    
	inst:AddComponent("follower")
	
    inst:SetStateGraph("SGhoodini")	
    local brain = require "brains/hoodinibrain"
    inst:SetBrain(brain)


    local light = inst.entity:AddLight()
    light:SetFalloff(.7)
    light:SetIntensity(.75)
    light:SetRadius(2.5)
    light:SetColour(120/255, 120/255, 120/255)
    light:Enable(false)

   -- inst:DoTaskInTime(0.1, function() spawnfx(inst) end)

    inst.DoLightning = DoLightning
    MakeMediumFreezableCharacter(inst, "body")
    MakeMediumBurnableCharacter(inst, "body")

    inst:WatchWorldState("isday", ReturnHome)
	inst:ListenForEvent("death", OnDeath)
    inst.components.burnable.lightningimmune = true
    
	 inst:AddComponent("lootdropper")
	 inst.components.lootdropper:SetLoot({"drumstick", "nightmarefuel"})

	inst:DoPeriodicTask(.3, StalkLeader)
	
    return inst
end

return Prefab( "forest/animals/hoodini", fn, assets, prefabs)