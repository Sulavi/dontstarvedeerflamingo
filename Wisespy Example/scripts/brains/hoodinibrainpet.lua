require "behaviours/follow"
require "behaviours/wander"

local TARGET_FOLLOW_DIST = 4
local MAX_FOLLOW_DIST = 4.5

local function GetLeader(inst)
    return inst.components.follower ~= nil and inst.components.follower.leader or nil
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower ~= nil and inst.components.follower.leader == target
end
--hoodinibrain
local HoodiniBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function HoodiniBrain:OnStart()
    local root = PriorityNode(
    {
        WhileNode(function() return self.inst.components.hauntable and self.inst.components.hauntable.panic end, "PanicHaunted", Panic(self.inst)),
        Follow(self.inst, function() return self.inst.components.follower.leader end, 0, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST),
        FaceEntity(self.inst, GetLeader, KeepFaceTargetFn),
        Wander(self.inst),
    }, .25)

    self.bt = BT(self.inst, root)
end

return HoodiniBrain
