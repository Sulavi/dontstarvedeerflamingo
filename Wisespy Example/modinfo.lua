
name = " Whispering Wisespy"
version = "1.2"
description = "A mod that adds a new creature to the game, the Wisespy. The Whispering Wisespy Is an owl-like creature that lives in trees around the constant. They're mostly harmless but they will keep an eye on you. \n\nVersion: "..version.."\n-Added the option to let Living-trees spawn Wisespies aswell.\n-Wisespies may drive you mad if your mental state is weak.\n-Added a Minimap icon for the dens.\n\nMake sure you're up to date!\n\nA Creature mod created by Bretticus Von Magnus. I also take modding commissions! Feel free to contact me if interested."
author = "Sir Bretticus"


forumthread = ""

api_version = 10
priority = 5

--This lets the clients know that they need to download the mod before they can join a server that is using it.
all_clients_require_mod = true

--This let's the game know that this mod doesn't need to be listed in the server's mod listing
client_only_mod = false

--Let the mod system know that this mod is functional with Don't Starve Together
dont_starve_compatible = true
reign_of_giants_compatible = true
dst_compatible = true

--These tags allow the server running this mod to be found with filters from the server listing screen
server_filter_tags = {"Wisespy, owl, creature, bretticus, sir, wise, spy, hoot"}

icon_atlas = "modicon.xml"
icon = "modicon.tex"

configuration_options =
{
	{
		name = "LivingTreeNests",
		label = "Livingtree Nests",
		hover = "Should Totaly Normal Trees (Livingtrees) spawn Wisespies?",
		options =	{
						{description = "Yep", data = 1, hover = ""},
						{description = "Nope", data = 0, hover = ""},
					},
		default = 0,
	},
}
