Map = {}
 
modimport("setpiece.lua") -- replace directory with location setpiece.lua is located.
 
--[[
    This function acts as a wrapper which we will use to add our custom layout as a set piece.
    ------------------------------------------------------------------------------------------------------------
    level_id        -    Level Identification (LEVELTYPE.ALL, LEVELTYPE.SURVIVAL, LEVELTYPE.CAVE, LEVELTYPE.ADVENTURE, 
                        LEVELTYPE.TEST, LEVELTYPE.UNKNOWN, LEVELTYPE.CUSTOM)
    layout_name     -    The name of the layout which we will be adding, if the layout has not already been added we will add the layout.
    layout_type            -    Layout Type (LAYOUT_TYPE.RANDOM, LAYOUT_TYPE.GROUND, LAYOUT_TYPE.ROOM) Default is LAYOUT_TYPE.RANDOM.
    count            -    The amount of set pieces we want to spawn. Default is 1.
    chance            -    The percentage change the layout will spawn. Default is 100.
    terrain            -    Identification of the ground or room. Identification must coincide with the type or it will error out.
                        Ground Example: GROUND.GRASS or GROUND.DIRT
                        Room Example: Badlands or BGGrass
--]]
 --Room
-- The layout will automatically be loaded with this function, as long as the static_layout is located
-- in the proper folder directory: scripts/map/static_layouts
--local tasks={"Atrium"}
local tasks={"TheLabyrinth"}
--Map.CreateSetPiece( Map.LEVELTYPE.ALL, "charlie_arena", Map.LAYOUT_TYPE.GROUND, 1, 100, "GROUND.IMPASSABLE" )
local tasks={"Necronomicon"}

Map.CreateSetPiece( Map.LEVELTYPE.ALL, "hoodiniden", Map.LAYOUT_TYPE.ROOM, 1, 100, "Graveyard" )
Map.CreateSetPiece( Map.LEVELTYPE.ALL, "hoodiniden", Map.LAYOUT_TYPE.ROOM, 8, 30, "DeepForest" )