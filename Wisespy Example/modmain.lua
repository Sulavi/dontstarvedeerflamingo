local require = GLOBAL.require
local TheInput = GLOBAL.TheInput
local ThePlayer = GLOBAL.ThePlayer
local IsServer = GLOBAL.TheNet:GetIsServer()
local Inv = require "widgets/inventorybar"
local _G = GLOBAL

local RECIPETABS = GLOBAL.RECIPETABS
local Ingredient = GLOBAL.Ingredient
local TECH = GLOBAL.TECH
local STRINGS = GLOBAL.STRINGS

PrefabFiles = {
 "hoodini",
 "hoodiniden",
 "hoodinipet",
}

Assets =
{   
	Asset("ANIM", "anim/greatlivingtree.zip"),
	Asset("ANIM", "anim/hoodini.zip"),

	Asset("IMAGE", "images/inventoryimages/hoodini.tex"),
    Asset("ATLAS", "images/inventoryimages/hoodini.xml"),
	
	Asset("SOUNDPACKAGE", "sound/owlsounds.fev"),
    Asset("SOUND", "sound/owlsounds.fsb"),
}


local LivingTreeNests = GetModConfigData("LivingTreeNests")
LivingTreeNests = LivingTreeNests == 1 or LivingTreeNests == true or LivingTreeNests == "true"


local function fn(inst)
local function ReturnChildren(inst)
    for k, child in pairs(inst.components.childspawner.childrenoutside) do
        if child.components.homeseeker ~= nil then
            child.components.homeseeker:GoHome()
        end
		child.components.follower:StopFollowing()
        child:PushEvent("gohome")
    end
end

local function OnIsDay(inst, isday)
    if isday then
        inst.components.childspawner:StartRegen()
        inst.components.childspawner:StopSpawning()
        ReturnChildren(inst)
		--inst.AnimState:PlayAnimation("owly_tree_empty")
    else
        inst.components.childspawner:StopRegen()
        inst.components.childspawner:StartSpawning()
		--inst.AnimState:PlayAnimation("owly_tree")
    end
end

	inst:AddTag("hoodiniden")
	
	inst:AddComponent("childspawner")
    inst.components.childspawner:SetRegenPeriod(60)
    inst.components.childspawner:SetSpawnPeriod(.1)
    inst.components.childspawner:SetMaxChildren(1)
    inst.components.childspawner:SetSpawnedFn( onspawnchild )
    inst.components.childspawner:SetGoHomeFn( childgohome )
    inst.components.childspawner.childname = "hoodini"

    OnIsDay(inst, GLOBAL.TheWorld.state.isday)
    inst:WatchWorldState("isday", OnIsDay)
end



if LivingTreeNests then
AddPrefabPostInit("livingtree", fn)	
end

AddRecipe("hoodinipetpetbuilder", {Ingredient("livinglog", 3), Ingredient("mandrakesoup", 1)}, GLOBAL.RECIPETABS.ORPHANAGE, GLOBAL.TECH.ORPHANAGE_ONE, nil, nil, true, nil, nil, "images/inventoryimages/hoodini.xml", "hoodini.tex" )

STRINGS.NAMES.HOODINIPETPETBUILDER = "Wiseling"
STRINGS.RECIPE_DESC.HOODINIPETPETBUILDER = "Befriend a whispering Wiseling."

STRINGS.NAMES.HOODINI = "Wisespy"


STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOODINI = "I've deducted that i'd better stay away from them."
STRINGS.CHARACTERS.WENDY.DESCRIBE.HOODINI = "They'll stare straight into your soul."
STRINGS.CHARACTERS.WX78.DESCRIBE.HOODINI = "THEY KEEP DISAPPEARING."
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.HOODINI = "Not one of my creations, but I don't like them.."
STRINGS.CHARACTERS.WEBBER.DESCRIBE.HOODINI = "They keep following us!"
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.HOODINI = "Creepy eyes frighten Wolfgang."
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.HOODINI = "Those birds are not to be trusted!"
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.HOODINI = "I've never seen a specimen quite this large."
STRINGS.CHARACTERS.WILLOW.DESCRIBE.HOODINI = "Let's ruffle some feathers."
STRINGS.CHARACTERS.WOODIE.DESCRIBE.HOODINI = "You wont mind if I chop down that nest of yours, eh?"

STRINGS.NAMES.HOODINIPET = "Wiseling"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOODINIPET = "Will you help me do science, feathered friend?"
STRINGS.CHARACTERS.WENDY.DESCRIBE.HOODINIPET = "You look like you've seen the other side."
STRINGS.CHARACTERS.WX78.DESCRIBE.HOODINIPET = "YOU LOOK EVIL. I APPROVE."
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.HOODINIPET = "It's you and me against the world pal."
STRINGS.CHARACTERS.WEBBER.DESCRIBE.HOODINIPET = "We think they're both cute and creepy."
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.HOODINIPET = "Scary, not too scary."
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.HOODINIPET = "Thou shall be my eyes in the sky!"
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.HOODINIPET = "It's smaller than the other one's ive seen."
STRINGS.CHARACTERS.WILLOW.DESCRIBE.HOODINIPET = "Burning their nests might be fun."
STRINGS.CHARACTERS.WOODIE.DESCRIBE.HOODINIPET = "Instead of living in a tree, you're now with me, eh?"


STRINGS.NAMES.HOODINIDEN = "Suspiciously Large Evergreen"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOODINIDEN = "W-hooo do you think you're fooling?"
STRINGS.CHARACTERS.WX78.DESCRIBE.HOODINIDEN = "OPTICAL CENSORS WON'T BE FOOLED, I KNOW YOU'RE IN THERE."
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.HOODINIDEN = "Big bird not fooling anyone."
STRINGS.CHARACTERS.WOODIE.DESCRIBE.HOODINIDEN = "Now that's what i call a tree."
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.HOODINIDEN = "That tree is a bit bigger than i remember."
STRINGS.CHARACTERS.WEBBER.DESCRIBE.HOODINIDEN = "We promise not to hurt you if you don't hurt us!"
STRINGS.CHARACTERS.WENDY.DESCRIBE.HOODINIDEN = "It sends a chill down my spine for some reason."
STRINGS.CHARACTERS.WILLOW.DESCRIBE.HOODINIDEN = "Don't make me set your nest on fire, i know you're in there!"
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.HOODINIDEN = "Quite a sturdy Evergreen."
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.HOODINIDEN = "It's not the world tree but still quite large."