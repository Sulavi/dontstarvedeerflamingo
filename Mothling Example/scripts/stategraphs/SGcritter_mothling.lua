require("stategraphs/commonstates")
require("stategraphs/SGcritter_common")

local actionhandlers =
{
}

local events =
{
    SGCritterEvents.OnEat(),
    SGCritterEvents.OnAvoidCombat(),
	SGCritterEvents.OnTraitChanged(),

    CommonHandlers.OnSleepEx(),
    CommonHandlers.OnWakeEx(),
    CommonHandlers.OnLocomote(false,true),
}

local states =
{
}

local emotes =
{
	{ anim="emote_wiggle",
      timeline=
		{
			TimeEvent(13*FRAMES, function(inst)  inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
			
		},
	},
	{ anim="emote_rub",
      timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
		},
	},
	{ anim="emote_nuzzle",
      timeline=
		{
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
		},
	},
}

SGCritterStates.AddIdle(states, #emotes)
SGCritterStates.AddRandomEmotes(states, emotes)
SGCritterStates.AddEmote(states, "cute", 
		{
			
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
			
		})
SGCritterStates.AddCombatEmote(states,
		{
			pre =
			{
				TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
			},
			loop =
			{
				TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/angry") end),
			},
			pst =
			{
				TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
			},
		})
SGCritterStates.AddPlayWithOtherCritter(states, events,
	{
		active =
		{
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
		},
		passive = 
		{
			TimeEvent(57*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
		},
	})
SGCritterStates.AddEat(states,
        {
          --  TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/eat_pre") end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),

        --    TimeEvent((28+0)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/eat") end),
        --    TimeEvent((28+10)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/eat") end),
            
        --    TimeEvent((28+24+0)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/eat") end),
          --  TimeEvent((28+24+6)*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/blink") end),
        })
SGCritterStates.AddHungry(states,
        {
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/happy") end),
           -- TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/angry") end),
          --  TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/angry") end),
           -- TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/angry") end),
        })
SGCritterStates.AddNuzzle(states, actionhandlers,
		{
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
        })

SGCritterStates.AddWalkStates(states, nil, true)

local function StartFlapping(inst)
    inst.SoundEmitter:PlaySound("mothling/sound/flap", "flying")
end

local function RestoreFlapping(inst)
    if not inst.SoundEmitter:PlayingSound("flying") then
        StartFlapping(inst)
    end
end

local function StopFlapping(inst)
    inst.SoundEmitter:KillSound("flying")
end

local function CleanupIfSleepInterrupted(inst)
    if not inst.sg.statemem.continuesleeping then
        RestoreFlapping(inst)
    end
end

SGCritterStates.AddPetEmote(states, 
		{
			--TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/blink") end),
            TimeEvent(4*FRAMES, StopFlapping),
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
            TimeEvent(27*FRAMES, StartFlapping),
		},
        RestoreFlapping)

CommonStates.AddSleepExStates(states,
		{
			starttimeline =
			{
				TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
                TimeEvent(44*FRAMES, StopFlapping),
				TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
				
			},
			sleeptimeline = 
			{
				TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
				TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:PlaySound("mothling/sound/squeak") end),
			},
			endtimeline = 
			{
				TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/dragonling/blink") end),
                TimeEvent(12*FRAMES, StartFlapping),
			},
		},
        {
            onexitsleep = CleanupIfSleepInterrupted,
            onexitsleeping = CleanupIfSleepInterrupted,
            onexitwake = RestoreFlapping,
            onwake = StopFlapping,
        })

return StateGraph("SGcritter_mothling", states, events, "idle", actionhandlers)
