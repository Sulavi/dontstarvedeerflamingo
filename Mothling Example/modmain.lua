--local Spot = GetModConfigData("Spot")

PrefabFiles = {
"mothling",
}

Assets = {
Asset( "ATLAS", "images/inventoryimages/mothling.xml" ),
Asset( "IMAGE", "images/inventoryimages/mothling.tex" ),

Asset("SOUND", "sound/mothling.fsb"),
Asset("SOUNDPACKAGE", "sound/mothling.fev"),
}


local FOODTYPE = GLOBAL.FOODTYPE
local CHARACTER_INGREDIENT = GLOBAL.CHARACTER_INGREDIENT
local AllRecipes = GLOBAL.AllRecipes
local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
local TECH = GLOBAL.TECH

local STRINGS = GLOBAL.STRINGS
----------------------------------------------------------------------
AddRecipe("critter_mothling_builder", 
{
Ingredient("butterflymuffin", 1),
Ingredient("butterflywings", 1)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/mothling.xml", 
"mothling.tex")

STRINGS.NAMES.CRITTER_MOTHLING_BUILDER = "Mothling"
STRINGS.RECIPE_DESC.CRITTER_MOTHLING_BUILDER = "A flying floofy friend."


----------------------------------------------------------------------

AddRecipe("critter_butterling_builder", 
{
Ingredient("butter", 1),
Ingredient("butterflywings", 1)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/mothling.xml", 
"butterling.tex")

STRINGS.NAMES.CRITTER_BUTTERLING_BUILDER = "Butterling"
STRINGS.RECIPE_DESC.CRITTER_BUTTERLING_BUILDER = "A flying skinny friend."


----------------------------------------------------------------------

AddRecipe("critter_bumbleling_builder", 
{
Ingredient("honey", 1),
Ingredient("stinger", 1)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/mothling.xml", 
"bumbleling.tex")

STRINGS.NAMES.CRITTER_BUMBLELING_BUILDER = "Bumbleling"
STRINGS.RECIPE_DESC.CRITTER_BUMBLELING_BUILDER = "A flying fat friend."


----------------------------------------------------------------------

AddRecipe("critter_cicadling_builder", 
{
Ingredient("trailmix", 1),
Ingredient("charcoal", 1)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/mothling.xml", 
"cicadling.tex")

STRINGS.NAMES.CRITTER_CICADLING_BUILDER = "Cicadling"
STRINGS.RECIPE_DESC.CRITTER_CICADLING_BUILDER = "A flying hard friend."


----------------------------------------------------------------------

STRINGS.NAMES.CRITTER_MOTHLING = "Mothling"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_MOTHLING = "Fuzziest of friends."

STRINGS.NAMES.CRITTER_BUTTERLING = "Butterling"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_BUTTERLING = "Irridescend friendliness."

STRINGS.NAMES.CRITTER_BUMBLELING = "Bumbeling"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_BUMBLELING = "It buzzes of happiness."

STRINGS.NAMES.CRITTER_CICADLING = "Cicadling"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_CICADLING = "Squeaking companion."