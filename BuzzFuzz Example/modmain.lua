local require = GLOBAL.require
local FOODTYPE = GLOBAL.FOODTYPE
local CHARACTER_INGREDIENT = GLOBAL.CHARACTER_INGREDIENT
local AllRecipes = GLOBAL.AllRecipes
local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
local TECH = GLOBAL.TECH

local STRINGS = GLOBAL.STRINGS

PrefabFiles = {
 "buzzfuzz",
}

-- CRAFTING recipes:
-- Buzzfuzz: 1 honeycomb, 1 butter muffin
-- Shopkit: 1 warez, 5 fur tufts

Assets = {
Asset( "ATLAS", "images/inventoryimages/buzzfuzz.xml" ),
Asset( "IMAGE", "images/inventoryimages/buzzfuzz.tex" ),
}
----------------------------------------------------------------------
AddRecipe("critter_buzzfuzz_builder", 
{
Ingredient("stinger", 3),
Ingredient("honey", 10)
},
RECIPETABS.ORPHANAGE,
TECH.ORPHANAGE_ONE,
nil, 
nil, 
true,
nil, 
nil,
 "images/inventoryimages/buzzfuzz.xml", 
"buzzfuzz.tex")

STRINGS.NAMES.CRITTER_BUZZFUZZ_BUILDER = "Buzzfuzz"
STRINGS.RECIPE_DESC.CRITTER_BUZZFUZZ_BUILDER = "Adopt an energetic Buzzfuzz."


----------------------------------------------------------------------
STRINGS.NAMES.CRITTER_BUZZFUZZ = "Buzzfuzz"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRITTER_BUZZFUZZ = {"A wibbly-wobbly ball of fuzzy-wuzzy.","My friend here is the bees' knees!"}

if STRINGS.CHARACTERS.DRTONY ~= nil then
	STRINGS.CHARACTERS.DRTONY.DESCRIBE.CRITTER_BUZZFUZZ = {"An aerodynamical conundrum. I like you.","Do you sting, I wonder?","Together, we shall be undefeatable.","A friendlier drone than the ones I'm used to."}
end
if STRINGS.CHARACTERS.DRACO ~= nil then
	STRINGS.CHARACTERS.DRACO.DESCRIBE.CRITTER_BUZZFUZZ = {"Friendlier than the rest. Best keep it that way.","You seem mild-mannered enough.","Do you like honey?"}
end

STRINGS.CHARACTERS.WENDY.DESCRIBE.CRITTER_BUZZFUZZ = {"This ball of fuzz cannot possibly survive in this cruel world.","What will you do, come winter?"}
STRINGS.CHARACTERS.WX78.DESCRIBE.CRITTER_BUZZFUZZ = {"IT REFUSES TO CEASE ITS BUZZING.","MARGINALLY LESS REPULSIVE THAN OTHERS OF ITS KIND."}
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.CRITTER_BUZZFUZZ = "It's sticky with honey. I better not let it near my suit."
STRINGS.CHARACTERS.WEBBER.DESCRIBE.CRITTER_BUZZFUZZ = {"I love your buzzing!","We'd like to pet you. Carefully."}
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.CRITTER_BUZZFUZZ = "Buzz thing has sweet honey smell."
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.CRITTER_BUZZFUZZ = {"This aerial beast is magnificent.","The patron of mead is deserving of flowers."}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.CRITTER_BUZZFUZZ = {"Uncommonly large and plump for a member of the Apis.","This one must be a drone, why else would it laze about so?"}
STRINGS.CHARACTERS.WILLOW.DESCRIBE.CRITTER_BUZZFUZZ = "Why, aren't you the sweetest thing!"
STRINGS.CHARACTERS.WOODIE.DESCRIBE.CRITTER_BUZZFUZZ = "At least you're not a wasp."
STRINGS.CHARACTERS.WARLY.DESCRIBE.CRITTER_BUZZFUZZ = {"You don't produce honey, do you? Then what's the use of you?","Even with no honey, you're quite good company."}
STRINGS.CHARACTERS.WORMWOOD.DESCRIBE.CRITTER_BUZZFUZZ = "Floaty buzz friend!"
STRINGS.CHARACTERS.WORTOX.DESCRIBE.CRITTER_BUZZFUZZ = "It is too large to land on most flowers."
----------------------------------------------------------------------
