
name = "Buzzfuzz"
version = "1.0"
description = "Adds a new craftable Critter to the game: the Buzzfuzz. An adorable, bumbly friend and distant relative of the Glomglom."
author = "Hekateras"


forumthread = ""

api_version = 10
priority = -1

--This lets the clients know that they need to download the mod before they can join a server that is using it.
all_clients_require_mod = true

--This let's the game know that this mod doesn't need to be listed in the server's mod listing
client_only_mod = false

--Let the mod system know that this mod is functional with Don't Starve Together
dont_starve_compatible = true
reign_of_giants_compatible = true
dst_compatible = true

--These tags allow the server running this mod to be found with filters from the server listing screen
server_filter_tags = {"critter","cat"}

icon_atlas = "modicon_buzzfuzz.xml"
icon = "modicon_buzzfuzz.tex"

-- configuration_options =
-- {
	-- {

	-- },
-- }
